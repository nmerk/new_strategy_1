#ifndef NEW_STRATEGY_1_PROJECT_INCLUDE_PAGE_HPP
#define NEW_STRATEGY_1_PROJECT_INCLUDE_PAGE_HPP

#include "vector"
#include "item.hpp"
#include "definitions.hpp"

using std::vector;

class Page {
 private:
    sf::RenderWindow *window_;
    int num_;
    Item items_[NUM_OF_ITEMS];
    int active_item_;
    sf::Vector2f size_;
    sf::Vector2f position_;
 public:
    Page() : active_item_(NO_ACTIVE_ITEMS) {}
    void setNum(int num);
    bool setWindow(sf::RenderWindow *window);
    void display();
    void setSizeByParent(sf::Vector2f parent_size);
    void setPositionByParent(sf::Vector2f parent_position);
    void setParametersOfItems();
};

#endif  // NEW_STRATEGY_1_PROJECT_INCLUDE_PAGE_HPP
