#ifndef NEW_PROJECT_2_GAME_HPP
#define NEW_PROJECT_2_GAME_HPP

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "player.hpp"
#include "field.hpp"

class Game {
 public:
    Game() = default;
    Game(sf::RenderWindow *window);

    ~Game() = default;

    void run();

    sf::RenderWindow *get_window() const;

    Player get_left_player() const;
    Player get_right_player() const;

    void set_left_player(const Player &player_1);
    void set_right_player(const Player &player_2);

    Field get_field() const;
    void set_field(Field &field);

    void update_has_finished();
    void update_players_hp();

    bool left_player_has_won();  // to check who has won the game: true - player_1, false - player_2

 private:
    sf::RenderWindow *window_pointer_;

    bool player_move;
    bool has_finished_;

    Player left_player_;
    Player right_player_;

    Field field_;

    void handle_events();
    void update();
    void render();
};

#endif  // NEW_PROJECT_2_GAME_HPP
