#ifndef NEW_STRATEGY_1_PROJECT_INCLUDE_TAB_HPP
#define NEW_STRATEGY_1_PROJECT_INCLUDE_TAB_HPP

#include "page.hpp"
#include "definitions.hpp"

class Tab {
 private:
    sf::RenderWindow *window_;
    int num_;
    Page pages_[NUM_OF_PAGES];
    int active_page_;
    sf::Vector2f size_;
    sf::Vector2f position_;
    sf::Vector2f header_size_;
    sf::Vector2f header_position_;
    sf::Text name_;
 public:
    Tab();
    void setNum(int num);
    bool setWindow(sf::RenderWindow *window);
    void display_header();
    void display();
    void setName(sf::Text name);
    void setHeaderSize(sf::Vector2f header_size);
    sf::Vector2f getHeaderSize() const;
    void setHeaderPosition(sf::Vector2f header_position);
    sf::Vector2f getHeaderPosition() const;
    void setSizeByParent(sf::Vector2f parent_size);
    void setPositionByParent(sf::Vector2f parent_position);
    void setParametersOfPages();
};

#endif  // NEW_STRATEGY_1_PROJECT_INCLUDE_TAB_HPP
