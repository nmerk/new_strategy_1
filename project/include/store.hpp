#ifndef NEW_STRATEGY_1_PROJECT_INCLUDE_STORE_HPP
#define NEW_STRATEGY_1_PROJECT_INCLUDE_STORE_HPP

#include "definitions.hpp"
#include "tab.hpp"
#include "definitions.hpp"

class Store {
 private:
    sf::RenderWindow *window_;
    Tab tabs_[NUM_OF_TABS];
    int active_tab_;
    sf::Vector2f size_;
    sf::Vector2f position_;
    sf::Font font_;
    std::string tabnames_[NUM_OF_TABS];
    sf::Text tabnames_sf_[NUM_OF_TABS];
    sf::Vector2f tabheadersizes_[NUM_OF_TABS];
    sf::Vector2f tabheaderpositions_[NUM_OF_TABS];
 public:
    Store(sf::RenderWindow *window);
    void display();
    // void makeNextTabActive();
};

#endif  // NEW_STRATEGY_1_PROJECT_INCLUDE_STORE_HPP
