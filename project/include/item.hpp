#ifndef NEW_STRATEGY_1_PROJECT_INCLUDE_ITEM_HPP
#define NEW_STRATEGY_1_PROJECT_INCLUDE_ITEM_HPP

#include "object.hpp"
#include "misting.hpp"
#include "mistborn.hpp"
#include "soldier.hpp"
#include "metal.hpp"

#include "definitions.hpp"

class Item {
 private:
    sf::RenderWindow *window_;
    int num_;
    int type_;
    Object *object_;
    sf::Vector2f size_;
    sf::Vector2f position_;
 public:
    Item(int num = 0, int type = 0) : num_(num), type_(type) {}
    void setNum(int num);
    void setType(int type);
    bool setWindow(sf::RenderWindow *window);
    void display();
    void setSizeByParent(sf::Vector2f parent_size);
    void setPositionByParent(sf::Vector2f parent_position);
};

#endif  // NEW_STRATEGY_1_PROJECT_INCLUDE_ITEM_HPP
