#include "field.hpp"

#include <stdio.h>

void Field::reload() {
    for (size_t i = 0; i < get_battle_field().size(); ++i) {
        for (size_t j = 0; j < get_battle_field()[i].size(); ++j) {
            if (get_battle_field()[i][j].get_belonding() > 2) {
                battle_field_[i][j].set_belonding(get_battle_field()[i][j].get_belonding() - 2);
            }
        }
    }
}

void Field::output_field_with_belonging() {
    for (auto it:battle_field_) {
        for (auto is:it) {
            std::cout << is.get_belonding() << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << std::endl;
}

void Field::set_Being_in_Cell(std::any being, size_t i, size_t j) {
    if (being.type().name() == std::string("P8Mistborn")) {
        std::any_cast<Mistborn *>(being)->set_coordinates(
            std::pair<float, float>(battle_field_[i][j].get_x(), battle_field_[i][j].get_y()));
        std::any_cast<Mistborn *>(being)->set_base_position(
            std::pair<float, float>(battle_field_[i][j].get_x(), battle_field_[i][j].get_y()));
        std::any_cast<Mistborn *>(being)->set_base_texture_rect(sf::IntRect(0, 0, BEING_WIDTH, BEING_HEIGHT));
        battle_field_[i][j].set_Being(*std::any_cast<Mistborn *>(being));
    } else if (being.type().name() == std::string("P7Misting")) {
        std::any_cast<Misting *>(being)->set_coordinates(
            std::pair<float, float>(battle_field_[i][j].get_x(), battle_field_[i][j].get_y()));
        std::any_cast<Misting *>(being)->set_base_position(
            std::pair<float, float>(battle_field_[i][j].get_x(), battle_field_[i][j].get_y()));
        std::any_cast<Misting *>(being)->set_base_texture_rect(sf::IntRect(0, 0, BEING_WIDTH, BEING_HEIGHT));
        battle_field_[i][j].set_Being(*std::any_cast<Misting *>(being));
    } else if (being.type().name() == std::string("P7Soldier")) {
        std::any_cast<Soldier *>(being)->set_coordinates(
            std::pair<float, float>(battle_field_[i][j].get_x(), battle_field_[i][j].get_y()));
        std::any_cast<Soldier *>(being)->set_base_position(
            std::pair<float, float>(battle_field_[i][j].get_x(), battle_field_[i][j].get_y()));
        std::any_cast<Soldier *>(being)->set_base_texture_rect(sf::IntRect(0, 0, BEING_WIDTH, BEING_HEIGHT));
        battle_field_[i][j].set_Being(*std::any_cast<Soldier *>(being));
    }
}

short unsigned int Field::melee_combat(size_t f_i, size_t f_j, size_t s_i, size_t s_j) {
    short int first_hp, second_hp, first_force, second_force;
    if (get_battle_field()[f_i][f_j].get_Mistborn()) {
        first_hp = get_battle_field()[f_i][f_j].get_Mistborn()->get_hp();
        first_force = get_battle_field()[f_i][f_j].get_Mistborn()->get_force();
    } else if (get_battle_field()[f_i][f_j].get_Misting()) {
        first_hp = get_battle_field()[f_i][f_j].get_Misting()->get_hp();
        first_force = get_battle_field()[f_i][f_j].get_Misting()->get_force();
    } else {  //  Soldier
        first_hp = get_battle_field()[f_i][f_j].get_Soldier()->get_hp();
        first_force = get_battle_field()[f_i][f_j].get_Soldier()->get_force();
    }
    if (get_battle_field()[s_i][s_j].get_Mistborn()) {
        second_hp = get_battle_field()[s_i][s_j].get_Mistborn()->get_hp();
        second_force = get_battle_field()[s_i][s_j].get_Mistborn()->get_force();
    } else if (get_battle_field()[s_i][s_j].get_Misting()) {
        second_hp = get_battle_field()[s_i][s_j].get_Misting()->get_hp();
        second_force = get_battle_field()[s_i][s_j].get_Misting()->get_force();
    } else {  //  Soldier
        second_hp = get_battle_field()[s_i][s_j].get_Soldier()->get_hp();
        second_force = get_battle_field()[s_i][s_j].get_Soldier()->get_force();
    }

    second_hp -= first_force;
    if (second_hp <= 0) {
        return ENEMY_KILLED;
    } else {
        battle_field_[s_i][s_j].update_being_hp(second_hp);
        first_hp -= second_force;
        if (first_hp <= 0) {
            return FRIEND_KILLED;
        }
    }
    battle_field_[f_i][f_j].update_being_hp(first_hp);
    return DRAW;
}

unsigned short int Field::make_a_move(sf::RenderWindow &window, size_t i, size_t j, unsigned short int mode) {
    unsigned short int end_of_move = NORMAL_MOVE;
    switch (mode) {
        case 0:  //  up
            if (i == 0) {
                break;
            }
            if (get_battle_field()[i - 1][j].get_type() == false) {
                break;
            }

            if (!get_battle_field()[i - 1][j].if_free()) {
                //  атака, ближний бой
                if (get_battle_field()[i - 1][j].get_type() > 0 &&
                    get_battle_field()[i - 1][j].get_belonding() != get_battle_field()[i][j].get_belonding()) {
                    unsigned short int result = melee_combat(i, j, i - 1, j);
                    switch (result) {
                        case ENEMY_KILLED:battle_field_[i - 1][j].delete_being();
                            end_of_move = STOP_MOVE;
                            break;
                        case FRIEND_KILLED:battle_field_[i][j].delete_being();
                            return STOP_MOVE;
                        default:return STOP_MOVE;
                    }
                } else {
                    break;
                }
            }

            if (get_battle_field()[i][j].get_Mistborn()) {
                Mistborn temp = *get_battle_field()[i][j].get_Mistborn();
                set_Being_in_Cell(&temp, i - 1, j);
            } else if (get_battle_field()[i][j].get_Misting()) {
                Misting temp = *get_battle_field()[i][j].get_Misting();
                set_Being_in_Cell(&temp, i - 1, j);
            } else {  //  Soldier
                Soldier temp = *get_battle_field()[i][j].get_Soldier();
                set_Being_in_Cell(&temp, i - 1, j);
            }
            battle_field_[i - 1][j].set_belonding(get_battle_field()[i][j].get_belonding());
            battle_field_[i][j].delete_being();
            battle_field_[i - 1][j].set_y_coord(battle_field_[i - 1][j].get_y());
            return end_of_move;
        case 1:  //  right
            if (j + 1 > battle_field_width_ - 1) {
                break;
            }
            if (get_battle_field()[i][j + 1].get_type() == false) {
                break;
            }

            if (!get_battle_field()[i][j + 1].if_free()) {
                //  атака, ближний бой
                if (get_battle_field()[i][j + 1].get_type() > 0 &&
                    get_battle_field()[i][j + 1].get_belonding() != get_battle_field()[i][j].get_belonding()) {
                    unsigned short int result = melee_combat(i, j, i, j + 1);
                    switch (result) {
                        case ENEMY_KILLED:battle_field_[i][j + 1].delete_being();
                            end_of_move = STOP_MOVE;
                            break;
                        case FRIEND_KILLED:battle_field_[i][j].delete_being();
                            return STOP_MOVE;
                        default:return STOP_MOVE;
                    }
                } else {
                    break;
                }
            }

            if (get_battle_field()[i][j].get_Mistborn()) {
                Mistborn temp = *get_battle_field()[i][j].get_Mistborn();
                set_Being_in_Cell(&temp, i, j + 1);
            } else if (get_battle_field()[i][j].get_Misting()) {
                Misting temp = *get_battle_field()[i][j].get_Misting();
                set_Being_in_Cell(&temp, i, j + 1);
            } else {  //  Soldier
                Soldier temp = *get_battle_field()[i][j].get_Soldier();
                set_Being_in_Cell(&temp, i, j + 1);
            }
            battle_field_[i][j + 1].set_belonding(get_battle_field()[i][j].get_belonding());
            battle_field_[i][j].delete_being();
            battle_field_[i][j + 1].set_y_coord(battle_field_[i][j + 1].get_y());
            return end_of_move;
        case 2:  //  down
            if (i + 1 > battle_field_height_ - 1) {
                break;
            }
            if (get_battle_field()[i + 1][j].get_type() == false) {
                break;
            }

            if (!get_battle_field()[i + 1][j].if_free()) {
                //  атака, ближний бой
                if (get_battle_field()[i + 1][j].get_type() > 0 &&
                    get_battle_field()[i + 1][j].get_belonding() != get_battle_field()[i][j].get_belonding()) {
                    unsigned short int result = melee_combat(i, j, i + 1, j);
                    switch (result) {
                        case ENEMY_KILLED:battle_field_[i + 1][j].delete_being();
                            end_of_move = STOP_MOVE;
                            break;
                        case FRIEND_KILLED:battle_field_[i][j].delete_being();
                            return STOP_MOVE;
                        default:return STOP_MOVE;
                    }
                } else {
                    break;
                }
            }

            if (get_battle_field()[i][j].get_Mistborn()) {
                Mistborn temp = *get_battle_field()[i][j].get_Mistborn();
                set_Being_in_Cell(&temp, i + 1, j);
            } else if (get_battle_field()[i][j].get_Misting()) {
                Misting temp = *get_battle_field()[i][j].get_Misting();
                set_Being_in_Cell(&temp, i + 1, j);
            } else {  //  Soldier
                Soldier temp = *get_battle_field()[i][j].get_Soldier();
                set_Being_in_Cell(&temp, i + 1, j);
            }
            battle_field_[i + 1][j].set_belonding(get_battle_field()[i][j].get_belonding());
            battle_field_[i][j].delete_being();
            battle_field_[i + 1][j].set_y_coord(battle_field_[i + 1][j].get_y());
            return end_of_move;

        case 3:  //  left
            if (j == 0) {
                break;
            }
            if (get_battle_field()[i][j - 1].get_type() == false) {
                break;
            }

            if (!get_battle_field()[i][j - 1].if_free()) {
                //  атака, ближний бой
                if (get_battle_field()[i][j - 1].get_type() > 0 &&
                    get_battle_field()[i][j - 1].get_belonding() != get_battle_field()[i][j].get_belonding()) {
                    unsigned short int result = melee_combat(i, j, i, j - 1);
                    switch (result) {
                        case ENEMY_KILLED:battle_field_[i][j - 1].delete_being();
                            end_of_move = STOP_MOVE;
                            break;
                        case FRIEND_KILLED:battle_field_[i][j].delete_being();
                            return STOP_MOVE;
                        default:return STOP_MOVE;
                    }
                } else {
                    break;
                }
            }

            if (get_battle_field()[i][j].get_Mistborn()) {
                Mistborn temp = *get_battle_field()[i][j].get_Mistborn();
                set_Being_in_Cell(&temp, i, j - 1);
            } else if (get_battle_field()[i][j].get_Misting()) {
                Misting temp = *get_battle_field()[i][j].get_Misting();
                set_Being_in_Cell(&temp, i, j - 1);
            } else {  //  Soldier
                Soldier temp = *get_battle_field()[i][j].get_Soldier();
                set_Being_in_Cell(&temp, i, j - 1);
            }
            battle_field_[i][j - 1].set_belonding(get_battle_field()[i][j].get_belonding());
            battle_field_[i][j].delete_being();
            battle_field_[i][j - 1].set_y_coord(battle_field_[i][j - 1].get_y());
            return end_of_move;
    }
    return 0;
}

void Field::cell_control(sf::RenderWindow &window, size_t i, size_t j) {
    unsigned short int distance_of_walk = 0;
    if (get_battle_field()[i][j].get_Mistborn()) {
        distance_of_walk = get_battle_field()[i][j].get_Mistborn()->get_distance_of_walk();
    } else if (get_battle_field()[i][j].get_Misting()) {
        distance_of_walk = get_battle_field()[i][j].get_Misting()->get_distance_of_walk();
    } else {  //  Soldier
        distance_of_walk = get_battle_field()[i][j].get_Soldier()->get_distance_of_walk();
    }
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
                battle_field_[i][j].set_belonding(get_battle_field()[i][j].get_belonding() + 2);
                return;
            }
            std::cout << "distance of walk: " << distance_of_walk << std::endl;
            if (distance_of_walk == 0) {
                battle_field_[i][j].set_belonding(get_battle_field()[i][j].get_belonding() + 2);
                return;
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
                unsigned short int result = make_a_move(window, i, j, 0);
                if (result == NORMAL_MOVE) {
                    --distance_of_walk;
                    --i;
                } else if (result == STOP_MOVE) {
                    battle_field_[i][j].set_belonding(get_battle_field()[i][j].get_belonding() + 2);
                    return;
                }
                break;
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
                unsigned short int result = make_a_move(window, i, j, 1);
                if (result == NORMAL_MOVE) {
                    --distance_of_walk;
                    ++j;
                } else if (result == STOP_MOVE) {
                    battle_field_[i][j].set_belonding(get_battle_field()[i][j].get_belonding() + 2);
                    return;
                }
                break;
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
                unsigned short int result = make_a_move(window, i, j, 2);
                if (result == NORMAL_MOVE) {
                    --distance_of_walk;
                    ++i;
                } else if (result == STOP_MOVE) {
                    battle_field_[i][j].set_belonding(get_battle_field()[i][j].get_belonding() + 2);
                    return;
                }
                break;
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
                unsigned short int result = make_a_move(window, i, j, 3);
                if (result == NORMAL_MOVE) {
                    --distance_of_walk;
                    --j;
                } else if (result == STOP_MOVE) {
                    battle_field_[i][j].set_belonding(get_battle_field()[i][j].get_belonding() + 2);
                    return;
                }
                break;
            }
        }
        window.clear(sf::Color::White);
        draw(window);
        window.display();
    }
}

void Field::catch_cursor(sf::RenderWindow &window, sf::Event &event, short unsigned int player_move) {
    if (event.type == sf::Event::MouseButtonPressed) {
        if (event.key.code == sf::Mouse::Left) {
            sf::Vector2i cursor_position = sf::Mouse::getPosition(window);
            if (cursor_position.x > horizontal_indentation_ &&
                cursor_position.x < window.getSize().x - horizontal_indentation_ &&
                cursor_position.y > upper_indentation_ && cursor_position.y < window.getSize().y - lower_indentation_) {
                size_t i = (cursor_position.y - upper_indentation_) / cell_width_,
                    j =
                    (cursor_position.x - horizontal_indentation_) / cell_width_;
                std::cout << "cursor in cell [ " << i << " , " << j << " ]" << std::endl;
                if (!get_battle_field()[i][j].if_free()) {
                    if (player_move + 1 == get_battle_field()[i][j].get_belonding()) {
                        cell_control(window, i, j);
                        //  TODO: либо найти нормальный способ обработки событий, либо подобрать адекватные координаты
                        sf::Mouse::setPosition(sf::Vector2i(0, 0), window);
                        //  борьба с отловом нескольких нажатий
                    }
                }
                std::cout << "end move" << std::endl;
                return;
            }
        }
    }
}

void Field::calculation_of_screen_parameters(sf::RenderWindow &window) {
    cell_width_ = window.getSize().y / 12;
    horizontal_indentation_ = window.getSize().x / 2 - cell_width_ * 6;
    upper_indentation_ = 80;
}

void Field::resize(sf::RenderWindow &window) {
    calculation_of_screen_parameters(window);
    double field_x_position = horizontal_indentation_;
    double field_y_position = upper_indentation_;
    for (auto i = 0; i < battle_field_height_; ++i) {
        for (auto j = 0; j < battle_field_width_; ++j) {
            battle_field_[i][j].set_coord(field_x_position, field_y_position);
            battle_field_[i][j].set_size(cell_width_, cell_width_);
            field_x_position += cell_width_;
        }
        field_x_position = horizontal_indentation_;
        field_y_position += cell_width_;
    }
}

double Field::get_horizontal_indentation() const {
    return horizontal_indentation_;
}

double Field::get_vertical_indentation() const {
    return upper_indentation_;
}

double Field::get_cell_width() const {
    return cell_width_;
}

void Field::output_field() {
    for (auto i = 0; i < battle_field_.size(); ++i) {
        for (auto j = 0; j < battle_field_[i].size(); ++j) {
            std::cout << battle_field_[i][j].get_x() << " " << battle_field_[i][j].get_y() << std::endl;
        }
        std::cout << std::endl;
    }
}

void Field::draw(sf::RenderWindow &window) {
    window.draw(background_sprite_);
    for (auto i = 0; i < battle_field_height_; ++i) {
        for (auto j = 0; j < battle_field_width_; ++j) {
            window.draw(battle_field_[i][j].get_sprite());
        }
    }
    //  Временно 2 цикла, позже пофиксится
    for (auto i = 0; i < battle_field_height_; ++i) {
        for (auto j = 0; j < battle_field_width_; ++j) {
            if (battle_field_[i][j].get_Mistborn()) {
                window.draw(battle_field_[i][j].get_Mistborn()->get_sprite());
            } else if (battle_field_[i][j].get_Misting()) {
                window.draw(battle_field_[i][j].get_Misting()->get_sprite());
            } else if (battle_field_[i][j].get_Soldier()) {
                window.draw(battle_field_[i][j].get_Soldier()->get_sprite());
            }
        }
    }
}

void Field::init_battle_field() {
    double field_x_position = horizontal_indentation_;
    double field_y_position = upper_indentation_;
    for (auto i = 0; i < battle_field_height_; ++i) {
        std::vector<Cell> buf_bf_line;
        for (auto j = 0; j < battle_field_width_; ++j) {
            Cell temp_cell(field_x_position, field_y_position, cell_width_, cell_width_, 1, "test_map");
            buf_bf_line.push_back(temp_cell);
            field_x_position += cell_width_;
        }
        field_x_position = horizontal_indentation_;
        field_y_position += cell_width_;
        battle_field_.push_back(buf_bf_line);
    }
    lower_indentation_ = field_y_position;
}

void Field::placement() {
    //  ТЕСТ (координаты?)
    int j = 0;
    for (size_t i = 0; i < players.size(); ++i) {
        for (auto it : players[i].get_mistborns()) {
            set_Being_in_Cell(&it, 0, j);
            battle_field_[0][j].set_belonding(i + 1);
        }
        for (auto it : players[i].get_soldiers()) {
            set_Being_in_Cell(&it, 4, j);
            battle_field_[4][j].set_belonding(i + 1);
        }
        for (auto it : players[i].get_mistings()) {
            set_Being_in_Cell(&it, 8, j);
            battle_field_[8][j].set_belonding(i + 1);
        }
        players[i].clear_beings_vector();
        j += 11;
    }
    generation_of_obstacles();  //  TODO учитывать занятые клетки
}

Field::Field(sf::RenderWindow &window, Player &left, Player &right) {
    if (!background_texture_.loadFromFile(PATH_TO_BACKGROUND))
        std::cout << "UNABLE TO LOAD BACKGROUND" << std::endl;

    background_sprite_.setTexture(background_texture_);

    players.push_back(left);
    players.push_back(right);
    calculation_of_screen_parameters(window);
    init_battle_field();
    placement();
    lower_indentation_ = window.getSize().y - lower_indentation_;
    output_field_with_belonging();
}

void Field::generation_of_obstacles() {
    std::default_random_engine generator;
    for (auto i = 0; i < NUMBER_OF_OBSTACLES; ++i) {
        std::uniform_int_distribution<int> x(2, BATTLEFIELD_WIDTH - 3);
        std::uniform_int_distribution<int> y(0, BATTLEFIELD_HEIGHT - 1);
        size_t y_it = y(generator), x_it = x(generator);
        while (battle_field_[y_it][x_it].get_belonding() != 0) {
            y_it = y(generator);
            x_it = x(generator);
        }
        battle_field_[y_it][x_it].set_type(false);
    }
}

std::vector<std::vector<Cell>> Field::get_battle_field() const {
    return battle_field_;
}

unsigned int Field::get_battle_field_width() const {
    return battle_field_width_;
}

unsigned int Field::get_battle_field_height() const {
    return battle_field_height_;
}

Field &Field::operator=(const Field &other) {

    horizontal_indentation_ = other.horizontal_indentation_;
    upper_indentation_ = other.upper_indentation_;
    lower_indentation_ = other.lower_indentation_;
    cell_width_ = other.cell_width_;

    background_texture_ = other.background_texture_;
    background_sprite_.setTexture(background_texture_);

    for (const auto &i : other.battle_field_) {
        battle_field_.push_back(i);
    }

    for (const auto &i : other.players) {
        players.push_back(i);
    }

    return *this;
}
