#include "store.hpp"

Store::Store(sf::RenderWindow *window) {
    // font loading
    if (!font_.loadFromFile(STORE_FONT)) {
        std::cerr << "Error while loading font" << std::endl;
    }

    // store parameters 
    window_ = window;
    active_tab_ = 0;

    float width = (float) window_->getSize().x;
    float height = (float) window_->getSize().y;

    size_ = sf::Vector2f(width /** 0.9*/, height /** 0.9*/);
    position_ = sf::Vector2f(0/*width * 0.05*/, 0/*height * 0.05*/);

    // tabs headers parameters
    for (int i = 0; i < NUM_OF_TABS; i++) {
        tabnames_sf_[i].setString(TABNAMES[i]);
        tabnames_sf_[i].setFont(font_);
        tabnames_sf_[i].setCharacterSize(SIZE_OF_TEXT);
        tabs_[i].setWindow(window_);
        tabs_[i].setNum(i);
        tabs_[i].setName(tabnames_sf_[i]);
        tabs_[i].setHeaderSize(sf::Vector2f(tabnames_sf_[i].getLocalBounds().width, 30.f));
    }

    sf::Vector2f tab_header_position;
    for (int i = 0; i < NUM_OF_TABS; i++) {
        tab_header_position.x = position_.x + GAP;
        for (int j = 0; j < i; j++) {
            tab_header_position.x = tab_header_position.x + tabs_[j].getHeaderSize().x + GAP;
        }
        tab_header_position.y = position_.y + GAP;
        tabs_[i].setHeaderPosition(tab_header_position);
    }

    // tabs main_parts parameters
    for (int i = 0; i < NUM_OF_TABS; i++) {
        tabs_[i].setSizeByParent(size_);
        tabs_[i].setPositionByParent(position_);
    }
}

void Store::display() {
    window_->clear();

    sf::RectangleShape store_rect(size_);
    store_rect.setPosition(position_);
    store_rect.setOutlineColor(sf::Color::White);
    store_rect.setOutlineThickness(1.f);
    store_rect.setFillColor(sf::Color::Black);

    window_->draw(store_rect);

    for (int i = 0; i < NUM_OF_TABS; i++) {
        tabs_[i].display_header();
    }

    tabs_[active_tab_].display();
}

/*void Store::makeNextTabActive() {
    active_tab_++;
    if (active_tab_ == NUM_OF_TABS) {
        active_tab_ = 0;
    }
}*/
