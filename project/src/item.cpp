#include "item.hpp"

void Item::setNum(int num) {
    num_ = num;
}

void Item::setType(int type) {
    if (type > NUM_OF_TYPES - 1 || type < 0) {
        type = 0;
    }
    type_ = type;
}

void Item::setSizeByParent(sf::Vector2f parent_size) {
    size_ = sf::Vector2f(parent_size.x - 2 * GAP, (parent_size.y - (NUM_OF_ITEMS + 1) * GAP) / 3);
}

void Item::setPositionByParent(sf::Vector2f parent_position) {
    float displacement = GAP + num_ * (size_.y + GAP);
    position_ = parent_position + sf::Vector2f(GAP, displacement);
}

bool Item::setWindow(sf::RenderWindow *window) {
    if (window != NULL) {
        window_ = window;
        return true;
    }
    return false;
}

void Item::display() {
    sf::RectangleShape item_rect(size_);
    item_rect.setPosition(position_);
    item_rect.setOutlineColor(sf::Color::White);
    item_rect.setOutlineThickness(1.f);
    item_rect.setFillColor(sf::Color::Black);

    window_->draw(item_rect);
}
