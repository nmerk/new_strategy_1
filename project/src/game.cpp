#include "game.hpp"

Game::Game(sf::RenderWindow *window) : has_finished_(false), window_pointer_(window) {
    //  TEST
    Mistborn test1(100, 200, 9, 8, 3, 600, sf::String("test_mistborn_1"), sf::String("test_mistborn_1"));
    Soldier test2(100, 10, 9, 8, 2, 600, sf::String("test_soldier_1"), sf::String("test_soldier_1"));
    Misting test3(100, 50, 9, 8, 100, 600, sf::String("test_misting_1"), sf::String("test_misting_1"));
    Mistborn test4(100, 10, 9, 8, 3, 600, sf::String("test_mistborn_1"), sf::String("test_mistborn_1"));
    Soldier test5(100, 200, 9, 8, 2, 600, sf::String("test_soldier_1"), sf::String("test_soldier_1"));
    Misting test6(100, 50, 9, 8, 100, 600, sf::String("test_misting_1"), sf::String("test_misting_1"));
    //  Рука левого игрока
    left_player_.add_mistborn(test1);
    left_player_.add_soldier(test2);
    left_player_.add_misting(test3);
    //  Рука правого игрока
    right_player_.add_mistborn(test4);
    right_player_.add_soldier(test5);
    right_player_.add_misting(test6);

    Field new_field(*window, left_player_, right_player_);
    set_field(new_field);

    player_move = 0;
}

void Game::run() {
    while (window_pointer_->isOpen()) {
        handle_events();
        render();
    }
}

void Game::handle_events() {
    sf::Event event;
    while (window_pointer_->pollEvent(event)) {

        field_.catch_cursor(*window_pointer_, event, static_cast<short unsigned int> (player_move));
        if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::E) {
            player_move = (player_move + 1) % 2;
            std::cout << "player move : " << player_move << std::endl;
//            field_.output_field_with_belonging();
            if (player_move == 0) {
                field_.reload();
            }
        }
        if (event.type == sf::Event::Closed) {
            window_pointer_->close();
            has_finished_ = true;
            return;
        }
    }
}

//void Game::update() {
//  update_players_hp();
//  update_has_finished();
//}

void Game::render() {
    window_pointer_->clear();
    field_.draw(*window_pointer_);

    // left player
    sf::String left_player_name = "LEFT POPUG";
    //  left_player_.set_player_name(left_player_name, 60, AVATAR_VERT_INDENT + AVATAR_HEIGHT + PLAYER_NAME_VERT_INDENT);  // TODO(nmerk): move magic digits to definitions.hpp
    //  window_pointer_->draw(left_player_.get_player_name());
    left_player_.set_avatar_sprite(1);
    left_player_.set_avatar_position(AVATAR_HOR_INDENT, AVATAR_VERT_INDENT);
    window_pointer_->draw(left_player_.get_avatar_sprite());

    // right player
    sf::String right_player_name = "RIGHT POPUG";
    //  right_player_.set_player_name(right_player_name, 777, 220);
    //  window_pointer_->draw(right_player_.get_player_name());
    right_player_.set_avatar_sprite(2);
    right_player_.set_avatar_position(window_pointer_->getSize().x - AVATAR_HOR_INDENT - AVATAR_WIDTH, AVATAR_VERT_INDENT);
    window_pointer_->draw(right_player_.get_avatar_sprite());
    window_pointer_->display();
}

sf::RenderWindow *Game::get_window() const {
    return window_pointer_;
}

Player Game::get_left_player() const {
    return left_player_;
}
Player Game::get_right_player() const {
    return right_player_;
}

void Game::set_left_player(const Player &player) {
    left_player_ = player;
}
void Game::set_right_player(const Player &player) {
    right_player_ = player;
}

Field Game::get_field() const {
    return field_;
}

void Game::set_field(Field &field) {
    field_ = field;
}

bool Game::left_player_has_won() {
    return right_player_.has_lost();
}

void Game::update_has_finished() {
    has_finished_ = (left_player_.has_lost()) || (right_player_.has_lost());
}

void Game::update_players_hp() {
    left_player_.update_hp();
    right_player_.update_hp();
}