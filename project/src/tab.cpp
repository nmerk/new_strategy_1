#include "tab.hpp"

void Tab::setHeaderSize(sf::Vector2f header_size) {
    header_size_ = header_size;
}

sf::Vector2f Tab::getHeaderSize() const {
    return header_size_;
}

void Tab::setHeaderPosition(sf::Vector2f header_position) {
    header_position_ = header_position;
}

sf::Vector2f Tab::getHeaderPosition() const {
    return header_position_;
}

Tab::Tab() {
    active_page_ = 0;

    for (int i = 0; i < NUM_OF_PAGES; i++) {
        pages_[i].setWindow(window_);
        pages_[i].setNum(i);
    }

}

void Tab::setNum(int num) {
    num_ = num;
}

bool Tab::setWindow(sf::RenderWindow *window) {
    if (window != NULL) {
        window_ = window;
        return true;
    }
    return false;
}

void Tab::display_header() {
    sf::RectangleShape header(header_size_);
    header.setPosition(header_position_);
    header.setOutlineColor(sf::Color::White);
    header.setOutlineThickness(1.f);
    header.setFillColor(sf::Color::Black);

    window_->draw(header);

    name_.setPosition(header_position_.x, header_position_.y);
    window_->draw(name_);
}

void Tab::setSizeByParent(sf::Vector2f parent_size) {
    size_ = sf::Vector2f(parent_size.x - 2 * GAP, parent_size.y - header_size_.y - 3 * GAP);
}

void Tab::setPositionByParent(sf::Vector2f parent_position) {
    position_ = parent_position + sf::Vector2f(GAP, 2 * GAP + header_size_.y);
}

void Tab::display() {
    sf::RectangleShape main_part(size_);
    main_part.setPosition(position_);
    main_part.setOutlineColor(sf::Color::White);
    main_part.setOutlineThickness(1.f);
    main_part.setFillColor(sf::Color::Black);

    window_->draw(main_part);

    setParametersOfPages();

    pages_[active_page_].display();
}

void Tab::setName(sf::Text name) {
    name_ = name;
}

void Tab::setParametersOfPages() {
    for (int i = 0; i < NUM_OF_PAGES; i++) {
        pages_[i].setNum(i);
        pages_[i].setWindow(window_);
        pages_[i].setSizeByParent(size_);
        pages_[i].setPositionByParent(position_);
    }
}
