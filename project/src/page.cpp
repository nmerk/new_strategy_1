#include "page.hpp"

void Page::setSizeByParent(sf::Vector2f parent_size) {
    size_ = sf::Vector2f(parent_size.x - 2 * GAP, parent_size.y - 2 * GAP);
}

void Page::setPositionByParent(sf::Vector2f parent_position) {
    position_ = parent_position + sf::Vector2f(GAP, GAP);
}

void Page::setNum(int num) {
    num_ = num;
}

bool Page::setWindow(sf::RenderWindow *window) {
    if (window != NULL) {
        window_ = window;
        return true;
    }
    return false;
}

void Page::display() {
    sf::RectangleShape page_rect(size_);
    page_rect.setPosition(position_);
    page_rect.setOutlineColor(sf::Color::White);
    page_rect.setOutlineThickness(1.f);
    page_rect.setFillColor(sf::Color::Black);

    window_->draw(page_rect);

    setParametersOfItems();

    for (int i = 0; i < NUM_OF_ITEMS; i++) {
        items_[i].display();
    }
}

void Page::setParametersOfItems() {
    for (int i = 0; i < NUM_OF_ITEMS; i++) {
        items_[i].setNum(i);
        items_[i].setWindow(window_);
        items_[i].setSizeByParent(size_);
        items_[i].setPositionByParent(position_);
    }
}
