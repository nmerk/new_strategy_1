#include <iostream>

#include "store.hpp"

int main(int argc, char **argv) {
    sf::RenderWindow window(sf::VideoMode(1024/*DEFAULT_SCREEN_WIDTH*/, 600/*DEFAULT_SCREEN_HEIGHT*/), "Test");
    Store store(&window);

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }
        window.clear(sf::Color::White);
        store.display();
        window.display();
    }

    return 0;
}